local Message = require("Message")

local ItemsRenderer = {}

function ItemsRenderer.load()
   ItemsRenderer.images = {
      coal = love.graphics.newImage("assets/coal.png"),
      log  = love.graphics.newImage("assets/log.png"),
   }
   ItemsRenderer.boundingBox = {
      coal = { x = 4, y = 5, width = 12, height = 9 },
      log  = { x = 0, y = 5, width = 16, height = 8 }
   }
end

function ItemsRenderer.setItems(theItems)
   ItemsRenderer.items = theItems
end

function ItemsRenderer.getImageFromType(type)
   return ItemsRenderer.images[type]
end

function ItemsRenderer.draw(camera)
   for i, v in ItemsRenderer.items:each() do
      local vis, cx, cy = camera:compute(v.x, v.y, 16, 16)
      local image = ItemsRenderer.images[v.type]
      if vis and image ~= nil then
         love.graphics.draw(image, cx, cy)
      end
   end
end



function ItemsRenderer.getBoundingBoxInfo(type)
   return ItemsRenderer.boundingBox[type]
end


Message.addCallback(function(type, data)
   if type == "spawn.item" then
      ItemsRenderer.items:add(data.type, data.x, data.y, data.vx, data.vy)
   end
end)

return ItemsRenderer

