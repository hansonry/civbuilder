local NinePatch          = require("NinePatch")
local ObjectList         = require("ObjectList")
local Camera             = require("Camera")
local ItemPickupNotifier = require("ItemPickupNotifier")
local AxeAnimation       = require("AxeAnimation")
local Grid               = require("Grid")
local Map                = require("Map")
local MapRenderer        = require("MapRenderer")
local Plants             = require("Plants")
local PlantsRenderer     = require("PlantsRenderer")
local TreeFallAnimation  = require("TreeFallAnimation")
local Items              = require("Items")
local ItemsRenderer      = require("ItemsRenderer")
local Message            = require("Message")
local Simulation         = require("Simulation")

local images = {}
local window

function loadImages()
   images.colorBox = love.graphics.newImage("assets/colorBox.png")
   images.character  = love.graphics.newImage("assets/character.png")   
   images.uiWindow   = love.graphics.newImage("assets/uiWindow.png")
   local font = love.graphics.newFont("assets/kenpixel.ttf", 8)
   love.graphics.setFont(font)
   window = NinePatch:new(images.uiWindow, 4, 4, 12, 12)
end

local map
local itemPickupParticles = ObjectList:new()
local plants = Plants:new()
local items  = Items:new()

local character = {}

function character:give(item)
   local itemSet
   if self.itemIndex[item] == nil then
      itemSet = {
         type  = item,
         count = 0
      }
      table.insert(self.items, itemSet)
      self.itemIndex[item] = #self.items
   else
      itemSet = self.items[self.itemIndex[item]]
   end
   itemSet.count = itemSet.count + 1
end


function createMap(width, height, grassY)
   width = width or 20
   height = height or 20
   map = Map:new(width, height)
   grassY = grassY or 5
   
   for index, value, x, y in map:each() do
      if y < grassY then
         map[index] = { type = "sky" }
      elseif y == grassY then
         map[index] = { type = "grass", dug = false }
      else
         if math.random() < 0.20 then
            map[index] = { type ="coal",  dug = false}
         else
            map[index] = { type ="dirt",  dug = false}
         end
      end
   end
   
   for x = 1, map:getWidth() do
      if math.random() < 0.10 then
         plants:add("tree", x, grassY - 1)
      end
   end
end

local camera = Camera:new()


function love.load()
   love.graphics.setDefaultFilter("nearest", "nearest")

   loadImages()
   MapRenderer.load()
   PlantsRenderer.load()
   PlantsRenderer.setPlants(plants)
   ItemsRenderer.load()
   ItemsRenderer.setItems(items)
   AxeAnimation.load()
   createMap(400, 200, 50)
   character.x = 200 * 16
   character.y = 40  * 16
   character.speed = 60 -- In Pixels Per Second
   character.facing = "left"
   character.canDig = false
   character.items = {}
   character.itemIndex = {}
end

local fallingSpeed = 100
local shoveSpeed = 40


function distance(x, y)
   return math.sqrt(x * x + y * y)
end

function normalize(x, y)
   local dist = distance(x, y)
   if dist < 0.000001 then
      return 0, 0
   end
   return x / dist, y / dist
end


function love.update(dt)

   -- check falling
   local mx, my, msx, msy = Grid.toGrid(character.x, character.y, 16, 16)
   local tileX = (mx - 1) * 16
   local tileY = (my - 1) * 16
   local area = {
      center = { x = mx, y = my },
      north = { x = mx, y = my - 1 },
      northEast = { x = mx + 1, y = my - 1 },
      east = { x = mx + 1, y = my },
      southEast = { x = mx + 1, y = my + 1 },
      south = { x = mx, y = my + 1 },
      southWest = { x = mx - 1, y = my + 1 },
      west = { x = mx - 1, y = my },
      northWest = { x = mx - 1, y = my - 1 }
   }
   for k, v in pairs(area) do
      if map:hasCoords(v.x, v.y) then
         v.index = map:toIndex(v.x, v.y)
         v.data = map[v.index]
      end
      if v.data ~= nil then
         if v.data.type == "sky" then
            v.passible = true
         else
            v.passible = v.data.dug
         end
         
         if v.data.type == "dirt" or
            v.data.type == "grass"or 
            v.data.type == "coal" then
            v.diggable = character.canDig and not v.data.dug
         else
            v.diggable = false
         end
      else
         v.passible = false
         v.diggable = false
      end
      v.passibleOrDiggable = v.passible or v.diggable
   end
   

   
   local fallAmount
   local riseAmount
   local leftAmount
   local rightAmount
   
   if area.center.data.type == "sky" then
      local maxFallAmount
      if area.south.data.type == "sky" then
         maxFallAmount = 8
      elseif msy < 0 then
         maxFallAmount = -msy
      else
         maxFallAmount = 0
      end
      fallAmount = fallingSpeed * dt
      if fallAmount > maxFallAmount then
         fallAmount = maxFallAmount
      end
      character.y = character.y + fallAmount
      wasMovingUpDown = true
   else 
      fallAmount = 0
   end
   
   if love.keyboard.isDown("1") then
      character.canDig = false
   elseif love.keyboard.isDown("2") then
      character.canDig = true
   end
   
   if fallAmount == 0 then
      local leftPressed = love.keyboard.isDown("a")
      local rightPressed = love.keyboard.isDown("d")
      local upPressed = love.keyboard.isDown("w")
      local downPressed = love.keyboard.isDown("s")
      -- Character Direction Command x and y
      local CDCX = 0
      local CDCY = 0
      if leftPressed then
         CDCX = -1
         character.facing = "left"
      elseif rightPressed then
         CDCX = 1
         character.facing = "right"
      end

      if upPressed then
         CDCY = -1
      elseif downPressed then
         CDCY = 1
      end


      
      function tryToMove(dx, dy, amount)
      
         -- Normalized Character Direction Command x and y
         local NCDCX, NCDCY = normalize(dx, dy)

         -- Character Direction Distantce x and y
         local CDDX = NCDCX * amount
         local CDDY = NCDCY * amount

         -- New Position From Tile Center
         local NPFTCX = CDDX + msx
         local NPFTCY = CDDY + msy

         --print("x:", CDCX, NCDCX, CDDX, msx, NPFTCX)

         -- New Position Location Text
         local NPLT
         -- New Position Location Text Virtical (either "east" or "west" or nil)
         local NPLTV = nil
         -- New Position Location Text Horisontal (either "east" or "west" or nil)
         local NPLTH = nil
         if math.abs(NPFTCX) <= 4 and
            math.abs(NPFTCY) <= 4 then
            NPLT = "center"
         elseif math.abs(NPFTCX) > 4 and
                math.abs(NPFTCY) > 4 then
            if NPFTCX > 0 then
               if NPFTCY > 0 then
                  NPLT = "southEast"
                  NPLTV = "east"
                  NPLTH = "south"
               else
                  NPLT = "northEast"
                  NPLTV = "east"
                  NPLTH = "north"
               end
            else
               if NPFTCY > 0 then
                  NPLT = "southWest"
                  NPLTV = "west"
                  NPLTH = "south"
               else
                  NPLT = "northWest"
                  NPLTV = "west"
                  NPLTH = "north"
               end
            end
         elseif math.abs(NPFTCX) > math.abs(NPFTCY) then
            if NPFTCX > 0 then
               NPLT = "east"
            else
               NPLT = "west"
            end
         else
            if NPFTCY > 0 then
               NPLT = "south"
            else
               NPLT = "north"
            end
         end

         --print("Text:", NPLT, area[NPLT].x, area[NPLT].y)

         
         if (NPLTV == nil and area[NPLT].passibleOrDiggable) or
            (NPLTV ~= nil and area[NPLT].passible and
             area[NPLTV].passible and area[NPLTH].passible) then
            return true, CDDX, CDDY
         else
            return false
         end
      end
      local amount = dt * character.speed
      local worked, CDDX, CDDY = tryToMove(CDCX, CDCY, amount)
      if not worked then
         if math.abs(CDCX) > math.abs(CDCY) then
            worked, CDDX, CDDY = tryToMove(CDCX, 0, amount)
            if not worked and math.abs(CDCY) > 0.001 then
               worked, CDDX, CDDY = tryToMove(0, CDCX, amount)
            end
         else
            worked, CDDX, CDDY = tryToMove(0, CDCY, amount)
            if not worked and math.abs(CDCX) > 0.001 then
               worked, CDDX, CDDY = tryToMove(CDCX, 0, amount)
            end
         end
      end

      if worked then
         character.x = character.x + CDDX
         character.y = character.y + CDDY

         if area.center.diggable then
            area.center.data.dug = true
            local type = area.center.data.type
            if type == "coal" then
               local wx, wy = Grid.toWorldUpperLeft(mx, my)
               Message.send("spawn.item", {
                  type = "coal",
                  x = wx,
                  y = wy
               })
            end
         end
      end
   end
   
   plants:update(dt)

   -- Check for Item Pickup
   for i, v in items:each() do
      local d = math.abs(v.x - character.x) +
                math.abs(v.y - character.y)
      if d < 16 then
         items:markForRemoval(i)
         ItemPickupNotifier:add(v.type)
         character:give(v.type)
      end
   end
   items:postLoop()
   
   -- Simulate Items
   local area = {}
   for i, v in items:each() do
      local bb = ItemsRenderer.getBoundingBoxInfo(v.type)
      Simulation.kinematic(dt, map, v, area, bb)
   end

   ItemPickupNotifier:update(dt)
   AxeAnimation.update(dt)
   TreeFallAnimation.update(dt)
   if love.keyboard.isDown("e") then
      AxeAnimation.trigger()
   end
end

function love.draw()
   -- Scale 2x
   love.graphics.scale( 2, 2 )
  
   -- Setup Camera
   camera:setViewport(0, 0,
                      love.graphics.getWidth()  / 2,
                      love.graphics.getHeight() / 2)
   camera:setTarget(character.x + 8, character.y + 8)

   MapRenderer.draw(map, camera)
   PlantsRenderer.draw(camera)
   TreeFallAnimation.draw(camera)
   ItemsRenderer.draw(camera)
   
   -- Draw Chacracter
   local characterFlip
   local characterOffsetX
   local toolX, toolY
   if character.facing == "left" then
      characterFlip = -1
      characterOffsetX = 16
      toolX = 4
      toolY = 8
   else
      characterFlip = 1
      characterOffsetX = 0
      toolX = 12
      toolY = 8
   end
   local vis, cx, cy = camera:compute(character.x, character.y, 16, 16)
   love.graphics.draw(images.character, 
                      cx + characterOffsetX, 
                      cy, 
                      0, characterFlip, 1)
   AxeAnimation.draw(camera, 
                     character.x + toolX, 
                     character.y + toolY, 
                     character.facing)
   
   ItemPickupNotifier:draw(camera, character.x, character.y)
   
   -- Inventory
   if love.keyboard.isDown("f") then
      local x = 32
      local y = 32
      
      window:draw(x, y, 100, (#character.items * 16) + 8)
      for i, v in ipairs(character.items) do
         local ix = x + 4
         local iy = y + ((i - 1) * 16) + 4 
         local image = ItemsRenderer.getImageFromType(v.type)
         if image ~= nil then
            love.graphics.draw(image, ix, iy)
            love.graphics.print(": " .. tostring(v.count), ix + 18, iy + 2)
         end
      end
   
   end
   -- Debug Text
   window:draw(0, 0, 100, 20)
   love.graphics.print("Can Dig is: " .. tostring(character.canDig), 6, 4)
   --love.graphics.rectangle("line", camera.viewport.x, camera.viewport.y, camera.viewport.width, camera.viewport.height)
end
