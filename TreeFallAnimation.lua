local Message        = require("Message")
local Grid           = require("Grid")
local PlantsRenderer = require("PlantsRenderer")
local ObjectList     = require("ObjectList")
local Interpolation  = require("Interpolation")
local Tools          = require("Tools")
local TreeFallAnimation = {}

TreeFallAnimation.list = ObjectList:new()

local timeout = 0.5

function TreeFallAnimation.draw(camera)
   for i, v in TreeFallAnimation.list:each() do
      local wx, wy = Grid.toWorldUpperLeft(v.x, v.y)
      local vis, cx, cy = camera:compute(wx - 16, wy - 64, 32, 80) 
      if vis then
         local p = Interpolation.cubic(0, 1, 1, 1, v.timer / timeout)
         local yOffset = (1 - p) * 80 
         
         love.graphics.draw(PlantsRenderer.images.tree, cx, cy + yOffset, 0, 1, p)
      end
   end
end

function TreeFallAnimation.update(dt)
   for i, v in TreeFallAnimation.list:each() do
      if v.timer < dt then
         v.timer = 0
      else
         v.timer = v.timer - dt
      end

      if v.timer < timeout * v.dropsLeft / v.totalDrops then
         local p = v.timer / timeout
         local yOffset = p * 80 
         v.dropsLeft = v.dropsLeft - 1
         local wx, wy = Grid.toWorldUpperLeft(v.x, v.y)
         local vx, vy = Tools.randomVector(0, 100, 180, 360)
         Message.send("spawn.item", { 
            type = "log", 
            x = wx, 
            y = wy - yOffset,
            vx = vx,
            vy = vy
         })
      end

      if v.timer <= 0 then
         TreeFallAnimation.list:markForRemoval(i)
      end

   end
   TreeFallAnimation.list:postLoop()
end


Message.addCallback(function(type, data)
   if type == "plant.harvested" then
      local fall = {
         x     = data.x, 
         y     = data.y,
         type  = data.type,
         timer = timeout,
         totalDrops = 4
      }
      fall.dropsLeft = fall.totalDrops
      TreeFallAnimation.list:add(fall)
   end
end)

return TreeFallAnimation

