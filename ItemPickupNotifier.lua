local ObjectList    = require("ObjectList")
local Interpolation = require("Interpolation")
local ItemsRenderer = require("ItemsRenderer")
local ItemPickupNotifier = {}
local list = ObjectList:new()

local timeout = 1.5
local addTimeout = 0.5

function ItemPickupNotifier:add(item)
   local last = list:getLast()
   if last ~= nil and last.timer < addTimeout then
      last.count = last.count + 1
   else
      list:add({
         timer = 0,
         type  = item,
         count = 1
      })
   end
end

function ItemPickupNotifier:draw(camera, x, y)
   local vis, cx, cy = camera:compute(x + 16, y - 32, 16, 48)
   if vis then
      for i, v in list:each() do
         local p  = v.timer / timeout
         local yo = cy + 2 + Interpolation.cubic(32, 0, 0, 0, p)
         local image = ItemsRenderer.getImageFromType(v.type)
         love.graphics.print("+" .. tostring(v.count), cx, yo + 2)
         if image ~= nil then
            love.graphics.draw(image, cx + 13, yo)
         end
      end
   end
end

function ItemPickupNotifier:update(dt)
   for i, v in list:each() do
      v.timer = v.timer + dt
      if v.timer > timeout then
         list:markForRemoval(i)
      end
   end
   list:postLoop() 
end

return ItemPickupNotifier

