local Tools = {}

function Tools.randomVector(velMin, velMax, angMinDeg, angMaxDeg)
   local vel = math.random(velMin, velMax)
   local angle = math.random(angMinDeg, angMaxDeg)
   local rx = math.cos(angle * math.pi / 180) * vel
   local ry = math.sin(angle * math.pi / 180) * vel
   return rx, ry
   
end

return Tools
