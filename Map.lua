local Map = {}


Map.__index = function(table, value, y)
   local lookup = Map[value]
   if lookup == nil then
      if type(value) == "number" then
         value = math.floor(value)
         assert(value >= 0)
         assert(value < table.width * table.height)
         return table.data[value + 1]
      else
         return nil
      end
   else
      return lookup
   end
end

function Map:new(width, height, obj)
   obj = obj or {}
   setmetatable(obj, self)
   obj.width  = width
   obj.height = height
   obj.data = {}
   return obj
end

function Map:toIndex(x, y)
   x = math.floor(x)
   y = math.floor(y)
   assert(x >= 0)
   assert(y >= 0)
   assert(x < self.width)
   assert(y < self.height)
   return y * self.width + x
end

function Map:hasCoords(x, y)
   return x >= 0 and x < self.width and
          y >= 0 and y < self.height
end

function Map:hasIndex(index)
   return index >= 0 and index < self.width * self.height
end

function Map:getWidth()
   return self.width
end

function Map:getHeight()
   return self.height
end

function Map:toCoords(mapIndex)
   mapIndex = math.floor(mapIndex)
   assert(mapIndex >= 0)
   assert(mapIndex < self.width * self.height)
   local y = math.floor(mapIndex / self.width)
   local x = mapIndex - y * self.width
   return x, y
end

function Map:set(x, y, value)
   local index = self:toIndex(x, y)
   self.data[index + 1] = value
end

function Map:get(x, y)
   local index = self:toIndex(x, y)
   return self.data[index + 1]
end

function Map:each()
   function nextTile(self, index)
      index = index + 1
      if index < self.width * self.height then
         local x, y = self:toCoords(index)
         local value = self.data[index + 1]
         return index, value, x, y
      else
         return nil
      end
   end
   return nextTile, self, -1
end

return Map

