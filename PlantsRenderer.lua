local Grid    = require("Grid")
local UI      = require("UI")
local Message = require("Message")
local PlantsRenderer = {}


local plants

function PlantsRenderer.load()
   PlantsRenderer.images = {
      tree = love.graphics.newImage("assets/tree.png"),
      youngTree = love.graphics.newImage("assets/youngTree.png"),
      sapling = love.graphics.newImage("assets/sapling.png"),
   }
end

function PlantsRenderer.setPlants(thePlants)
   plants = thePlants
end

function PlantsRenderer.draw(camera)
   local images = PlantsRenderer.images
   -- Draw Trees
   for i, v in plants:each() do
      if v.growth == 0 then
         local gx, gy = Grid.toWorldUpperLeft(v.x, v.y)
         local vis, cx, cy = camera:compute(gx - 16, gy - 64, 48, 80)
         love.graphics.draw(images.tree, cx, cy)
         if v.type == "tree" and v.health < 100 then
            UI.simpleProgressBar(cx + 16, cy + 88, 16, 4, v.health / 100)
         end
      elseif v.growth == 1 then
         local gx, gy = Grid.toWorldUpperLeft(v.x, v.y)
         local vis, cx, cy = camera:compute(gx - 16, gy - 32, 48, 48)
         love.graphics.draw(images.youngTree, cx, cy)
         
      elseif v.growth == 2 then
         local gx, gy = Grid.toWorldUpperLeft(v.x, v.y)
         local vis, cx, cy = camera:compute(gx, gy, 16, 16)
         love.graphics.draw(images.sapling, cx, cy)
         
      end
   end
end

Message.addCallback(function(type, data)
   if type == "tool.swing.apex" then
      local gx, gy = Grid.toGrid(data.x, data.y)
      local harvestedPlant, downed = plants:chopAt(gx, gy)
      if harvestedPlant ~= nil then
         Message.send("plant.hit", harvestedPlant)
         if downed then
            Message.send("plant.harvested", harvestedPlant)
         end
      end
   end
end)

return PlantsRenderer


