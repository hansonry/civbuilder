local Interpolation = {}

function Interpolation.linear(s, e, p)
   return (e - s) * p + s
end

function Interpolation.quad(s, m1, e, p)
   local o1 = (m1 - s)  * p + s
   local o2 = (e  - m1) * p + m1
   return (o2 - o1) * p + o1
end

function Interpolation.cubic(s, m1, m2, e, p)
   local o1 = (m1 - s)  * p + s
   local o2 = (m2 - m1) * p + m1
   local o3 = (e  - m2) * p + m2

   local q1 = (o2 - o1) * p + o1
   local q2 = (o3 - o2) * p + o2
   
   return (q2 - q1) * p + q1 
end

return Interpolation

