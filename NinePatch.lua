local NinePatch = {}

NinePatch.__index = NinePatch

function setup(self)
   local sw, sh = self.image:getDimensions()
   
   local s = {
      col1w = self.x1,
      col2w = self.x2 - self.x1,
      col3w = sw - self.x2,
      row1h = self.y1,
      row2h = self.y2 - self.y1,
      row3h = sh - self.y2
   }
   
   self.quads = {
      nw = love.graphics.newQuad(0,       0,       s.col1w, s.row1h, sw, sh),
      n  = love.graphics.newQuad(self.x1, 0,       s.col2w, s.row1h, sw, sh),
      ne = love.graphics.newQuad(self.x2, 0,       s.col3w, s.row1h, sw, sh),
      w  = love.graphics.newQuad(0,       self.y1, s.col1w, s.row2h, sw, sh),
      c  = love.graphics.newQuad(self.x1, self.y1, s.col2w, s.row2h, sw, sh),
      e  = love.graphics.newQuad(self.x2, self.y1, s.col3w, s.row2h, sw, sh),
      sw = love.graphics.newQuad(0      , self.y2, s.col1w, s.row3h, sw, sh),
      s  = love.graphics.newQuad(self.x1, self.y2, s.col2w, s.row3h, sw, sh),
      se = love.graphics.newQuad(self.x2, self.y2, s.col3w, s.row3h, sw, sh)      
   }
   self.sizes = s
   
   
end

function NinePatch:new(image, x1, y1, x2, y2)
   local patch = {
      image = image,
      x1 = x1,
      y1 = y1,
      x2 = x2,
      y2 = y2
   }
   setmetatable(patch, self)
   setup(patch)
   return patch
end

function NinePatch:draw(x, y, width, height, drawFunction)
   drawFunction = drawFunction or love.graphics.draw
   
   local cWidth  = width  - (self.sizes.col1w + self.sizes.col3w)
   local cHeight = height - (self.sizes.row1h + self.sizes.row3h)
   
   drawFunction(self.image, self.quads.nw, x,                             y)
   if cWidth > 0 then
      drawFunction(self.image, self.quads.n,  x + self.sizes.col1w,          y, 0, cWidth / self.sizes.col2w, 1)
   end
   drawFunction(self.image, self.quads.ne, x + self.sizes.col1w + cWidth, y)
   if cHeight > 0 then
      drawFunction(self.image, self.quads.w,  x,                             y + self.sizes.row1h, 0, 1, cHeight / self.sizes.row2h)
      if cWidth > 0 then
         drawFunction(self.image, self.quads.c,  x + self.sizes.col1w,          y + self.sizes.row1h, 0, cWidth / self.sizes.col2w, cHeight / self.sizes.row2h)
      end
      drawFunction(self.image, self.quads.e,  x + self.sizes.col1w + cWidth, y + self.sizes.row1h, 0, 1, cHeight / self.sizes.row2h)
  end
   drawFunction(self.image, self.quads.sw, x,                             y + self.sizes.row1h + cHeight)
   if cWidth > 0 then
      drawFunction(self.image, self.quads.s,  x + self.sizes.col1w,          y + self.sizes.row1h + cHeight, 0, cWidth / self.sizes.col2w, 1)
   end
   drawFunction(self.image, self.quads.se, x + self.sizes.col1w + cWidth, y + self.sizes.row1h + cHeight)

end

return NinePatch
