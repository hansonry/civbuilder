local ObjectList = require("ObjectList")
local Plants = {}

Plants.__index = Plants

function Plants:new(obj)
   obj = obj or {}
   setmetatable(obj, self)
   obj.list = ObjectList:new(obj.list)
   return obj
end

function Plants:add(type, gridX, gridY)
   local plant = {
      type = type,
      x = gridX,
      y = gridY,
      growth = 0,
      timer = 0
   }
   if type == "tree" then
      plant.health = 100
   end
   self.list:add(plant)
end

local growthTimeout = 1

function Plants:chopAt(gridX, gridY)
   local found = nil
   local foundIndex
   for i, v in self.list:each() do
      if v.x == gridX and v.y == gridY then
         found = v
         foundIndex = i
         break
      end
   end
   local hitDammage = 40

   if found ~= nil and found.growth == 0 then
      if found.health < hitDammage then
         found.health = 0
      else
         found.health = found.health - hitDammage
      end

      if found.health <= 0 then
         found.growth = 3
         found.timer = growthTimeout
         return found, true
      end
      return found, false
   end
   return nil, false
end

function Plants:update(dt)
   for i, v in self.list:each() do
      if v.growth > 0 then
         if v.timer < dt then
            v.timer = 0
         else
            v.timer = v.timer - dt
         end
         
         if v.timer <= 0 then
            v.growth = v.growth - 1
            v.timer = growthTimeout
            if v.growth == 0 then
               v.health = 100
            end
         end
      end
   end
end

function Plants:each()
   return self.list:each()
end

return Plants

