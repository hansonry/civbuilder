local Grid = require("Grid")
local Simulation = {}

function SetPosition(obj, key, x, y)
   if obj[key] == nil then
      obj[key] = {}
   end
   local o = obj[key]
   o.x = x
   o.y = y
end

function GetArea(obj, map, cmx, cmy)
   --obj = obj or {}
   
   SetPosition(obj, "center",    cmx,     cmy)
   SetPosition(obj, "north",     cmx,     cmy - 1)
   SetPosition(obj, "northEast", cmx + 1, cmy - 1)
   SetPosition(obj, "east",      cmx + 1, cmy)
   SetPosition(obj, "southEast", cmx + 1, cmy + 1)
   SetPosition(obj, "south",     cmx,     cmy + 1)
   SetPosition(obj, "southWest", cmx - 1, cmy + 1)
   SetPosition(obj, "west",      cmx - 1, cmy)
   SetPosition(obj, "northWest", cmx - 1, cmy - 1)

   for k, v in pairs(obj) do
      v.ulx, v.uly = Grid.toWorldUpperLeft(v.x, v.y)
      if map:hasCoords(v.x, v.y) then
         v.index = map:toIndex(v.x, v.y)
         v.data = map[v.index]
      else
         v.index = nil
         v.data = nil
      end
      if v.data ~= nil then
         if v.data.type == "sky" then
            v.passible = true
            v.ground = false
         else
            v.passible = v.data.dug
            v.ground = true
         end
         
      else
         v.passible = false
      end
   end
   --return obj
end

function Simulation.kinematic(dt, map, object, area, bb)
   if bb == nil then
      bbXOffset = 0
      bbYOffset = 0
      bbWidth   = Grid.cell.width
      bbHeight  = Grid.cell.height
   else
      bbXOffset = bb.x
      bbYOffset = bb.y
      bbWidth   = bb.width
      bbHeight  = bb.height
   end
 
   local mx, my, mcx, mcy = Grid.toGrid(object.x + bbXOffset, object.y + bbYOffset, bbWidth, bbHeight)
   GetArea(area, map, mx, my)
   
   if not area.center.ground then
      if (not area.south.ground or mcy < Grid.cell.height - bbHeight) then
         local ax = 0
         local ay = 256
         
         local vx = object.vx or 0
         local vy = object.vy or 0
         
         vx = vx + ax * dt
         vy = vy + ay * dt
         
         object.x = object.x + vx * dt
         object.y = object.y + vy * dt
         
         object.vx = vx
         object.vy = vy
      else
         object.y = area.center.uly + Grid.cell.height - bbHeight - bbYOffset + 1
         object.x = math.floor(object.x + 0.5)
      end
   end
   
   
end


return Simulation
