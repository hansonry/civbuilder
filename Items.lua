local ObjectList = require("ObjectList")
local Items = {}

Items.__index = Items

function Items:new(obj)
   obj = obj or {}
   setmetatable(obj, self)
   obj.list = ObjectList:new(obj.list)
   return obj
end

function Items:add(type, x, y, vx, vy)
   self.list:add({
      type = type,
      x = x,
      y = y,
      vx = vx,
      vy = vy
   })
end

function Items:each()
   return self.list:each()
end

function Items:removeIndex(index)
   self.list:remove(index)
end

function Items:markForRemoval(index)
   self.list:markForRemoval(index)
end

function Items:postLoop()
   self.list:postLoop()
end

return Items

