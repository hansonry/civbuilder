local Message = require("Message")

local AxeAnimation = {}

local images = {}
local swingTimeout  = 0.25
local reloadTimeout = 0.40
local timer
local reportedMidelOfSwing = true
function AxeAnimation.load()
   images.axe = {
      deg0  = love.graphics.newImage("assets/axe0Deg.png"),
      deg45 = love.graphics.newImage("assets/axe45Deg.png")
   }
   timer = 0
end

function AxeAnimation.trigger()
   if timer <= 0 then
      timer = swingTimeout + reloadTimeout
      reportedMidelOfSwing = false
      Message.send("tool.swing.start")
   end
end
local hit = {
   x = 0,
   y = 0
}

function AxeAnimation.update(dt)
   
   if timer > dt then
      timer = timer - dt
   else
      timer = 0
   end

   if timer - reloadTimeout < swingTimeout / 2 and
      not reportedMidelOfSwing then
      reportedMidelOfSwing = true
      Message.send("tool.swing.apex", hit)
   end

end

function AxeAnimation.draw(camera, x, y, facing)
   hit.y = y
   if facing == "left" then
      hit.x = x - 6
   else
      hit.x = x + 6
   end
   local swingTimer = timer - reloadTimeout
   if swingTimer > 0 then
      local xScale
      local vis, cx, cy = camera:compute(x, y, 8, 8)
      if facing == "left" then
         xScale = -1
      else
         xScale = 1
      end
      -- This was all trial and error.
      if     swingTimer > swingTimeout * 0.75 then
         love.graphics.draw(images.axe.deg0,  cx - 4 * xScale, cy, -math.pi / 2 * xScale, xScale, 1)
      elseif swingTimer > swingTimeout * 0.5  then
         love.graphics.draw(images.axe.deg45, cx,              cy - 8, 0,                 xScale, 1)
      elseif swingTimer > swingTimeout * 0.25 then
         love.graphics.draw(images.axe.deg0,  cx,              cy - 4, 0,                 xScale, 1)
      else
         love.graphics.draw(images.axe.deg45, cx + 8 * xScale, cy, math.pi / 2 * xScale,  xScale, 1)
      end
   end

   --local vis, cx, cy = camera:compute(hit.x, hit.y, 2, 2)
   --love.graphics.rectangle("line", cx, cy, 2, 2)
end

return AxeAnimation

