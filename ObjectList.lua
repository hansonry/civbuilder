local ObjectList = {}

function clearTable(t)
   if t == nil then
      return {}
   end
   for k, v in pairs(t) do
      t[k] = nil
   end
   return t
end

ObjectList.__index = ObjectList

function ObjectList:new(obj)
   obj = obj or {}
   setmetatable(obj, self)
   obj.list       = clearTable(obj.list)
   obj.removeList = clearTable(obj.removeList)
   obj.addList    = clearTable(obj.addList)
   
   return obj
end

function ObjectList:add(item)
   table.insert(self.list, item)
end

function ObjectList:addAfterLoop(item)
   table.insert(self.addList, item)
end

function ObjectList:markForRemoval(index)
   table.insert(self.removeList, index)
end


function ObjectList:postLoop()
   if #self.removeList > 0 then
      table.sort(self.removeList, function (a, b) return a > b end)
      for i, v in ipairs(self.removeList) do
         table.remove(self.list, v)
      end
      clearTable(self.removeList)
   end

   if #self.addList > 0 then
      for i, v in ipairs(self.addList) do
         table.insert(self.list, v)
      end
      clearTable(self.addList)
   end
end

function ObjectList:clear()
   clearTable(self.list)
end

function ObjectList:get(index)
   return self.list[index]
end

function ObjectList:getLast()
   return self.list[#self.list]
end

function ObjectList:remove(item)
   local index = nil
   for i, v in ipairs(self.list) do
      if v == item then
         index = i
         break;
      end
   end

   if index ~= nil then
      table.remove(self.list, index)
   end
end

function ObjectList:removeIndex(index)
   table.remove(self.list, index)
end

function ObjectList:each()
   return ipairs(self.list)
end


return ObjectList

