local Message = {}

local callbacks = {}

function Message.addCallback(callback)
   table.insert(callbacks, callback)
end

function Message.send(type, data)
   --print("Send Message Type: " .. type)
   for i, v in ipairs(callbacks) do
      v(type, data)
   end
end


return Message

