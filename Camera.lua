local Camera = {}

Camera.__index = Camera

function Camera:compute(x, y, width, height)
   local cx = x - self.target.x
   local cy = y - self.target.y
   local visable
   if cx < -width or 
      cy < -height or
      cx > self.viewport.width or
      cy > self.viewport.height then
      visable = false
   else
      visable = true
   end
   return visable, cx + self.viewport.x, cy + self.viewport.y
end

function Camera:setTarget(x, y)
   assert(x ~= nil)
   assert(y ~= nil)
   self.target.x = x - self.viewport.width  / 2
   self.target.y = y - self.viewport.height / 2
end

function Camera:setViewport(x, y, width, height)
   assert(x      ~= nil)
   assert(y      ~= nil)
   assert(width  ~= nil)
   assert(height ~= nil)

   self.viewport.x      = x
   self.viewport.y      = y
   self.viewport.width  = width
   self.viewport.height = height
end

function Camera:new(x, y, width, height)
   local cam = {
      viewport = {
         x = 0,
         y = 0,
         width = 100,
         height = 100
      },
      target = {
         x = 0,
         y = 0,
      }
   }
   setmetatable(cam, self)
   if x ~= nil then
      cam:setViewport(x, y, width, height)
   end
   return cam
end


return Camera

