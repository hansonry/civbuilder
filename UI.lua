local UI = {}


function UI.simpleProgressBar(x, y, width, height, percent)
   local filledWidth = width * percent
   love.graphics.setColor(1, 0, 0)
   love.graphics.rectangle("fill", x, y, filledWidth, height)
   love.graphics.setColor(0, 0, 0)
   love.graphics.rectangle("fill", x + filledWidth, y, width - filledWidth, height)
   love.graphics.setColor(1, 1, 1)
   love.graphics.rectangle("line", x, y, width, height)
   
end

return UI

