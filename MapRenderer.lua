local Grid = require("Grid")
local MapRenderer = {}

local images

function MapRenderer.load()
   images = {
      backDirt   = love.graphics.newImage("assets/backDirt.png"),
      backGrass  = love.graphics.newImage("assets/backGrass.png"),
      coalInDirt = love.graphics.newImage("assets/coalInDirt.png"),
      grass      = love.graphics.newImage("assets/grass.png"),
      dirt       = love.graphics.newImage("assets/dirt.png"),
      sky        = love.graphics.newImage("assets/sky.png")
   }
end

function MapRenderer.draw(map, camera)
   for index, value, x, y in map:each() do
      local gx, gy = Grid.toWorldUpperLeft(x, y)
      local vis, cx, cy = camera:compute(gx, gy, Grid.cell.width, Grid.cell.height)
      if vis then
         local image = nil
         local data = map[index]
         if data.type == "sky" then
            image = images.sky
         elseif data.type == "grass" then
            if data.dug then
               image = images.backGrass
            else
               image = images.grass
            end
         elseif data.type == "dirt" then
            if data.dug then
               image = images.backDirt
            else
               image = images.dirt
            end
         elseif data.type == "coal" then
            if data.dug then
               image = images.backDirt
            else
               image = images.coalInDirt
            end
         end
         if image ~= nil then
            love.graphics.draw(image, cx, cy)
         end
      end
   end
end

return MapRenderer

