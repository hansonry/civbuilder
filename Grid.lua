local Grid = {}

local zeroX = 0
local zeroY = 0

Grid.cell = {
   width  = 16,
   height = 16
}

function Grid.setZeroUpperLeft(x, y)
   zeroX = x
   zeroY = y
end

function Grid.toWorldUpperLeft(gridX, gridY)
   gridX = math.floor(gridX)
   gridY = math.floor(gridY)
   return gridX * Grid.cell.width + zeroX, gridY * Grid.cell.height + zeroY
end

function Grid.toWorldCenter(gridX, gridY)
   gridX = math.floor(gridX)
   gridY = math.floor(gridY)
   return (gridX + 0.5) * Grid.cell.width + zeroX, (gridY + 0.5) * Grid.cell.height + zeroY
end

function Grid.toGrid(worldX, worldY, width, height)
   width = width or 0
   height = height or 0
   local mx = math.floor((worldX + (width  / 2)) / Grid.cell.width)
   local my = math.floor((worldY + (height / 2)) / Grid.cell.height)
   local shiftX = worldX - (mx * Grid.cell.width)
   local shiftY = worldY - (my * Grid.cell.height)
   return mx, my, shiftX, shiftY
end


return Grid

